#ifndef DYNAMISCHESSPEICHERDINGS_MY_ALLOC_H
#define DYNAMISCHESSPEICHERDINGS_MY_ALLOC_H

#include <stdint.h>
#include <stdbool.h>
#include "testit/my_system.h"

void init_my_alloc();
void* my_alloc(size_t s);
void my_free(void* ptr);

size_t* vrtl_ptr_2_real(size_t vrtl_ptr);
size_t real_ptr_2_vrtl(const size_t* real_ptr);
uint16_t tiles_to_end(uint64_t vrtl_ptr);

void print_list();
void print_tree();
void print_mantile(uint64_t man_tile);

void v_free(uint64_t data);

uint64_t get_r_child(uint64_t parent, int8_t depth);
uint64_t get_l_child(uint64_t parent, int8_t depth);
uint8_t get_node_val(uint64_t rel_addr);
uint64_t get_node_parent(uint64_t node, uint8_t depth);
uint64_t get_leaf_parent(uint64_t leaf);
size_t* get_mem_prt(uint64_t man_tile, size_t s);
uint64_t get_fitting_leaf(size_t s);
uint8_t update_man_tile(uint64_t* man_tile, bool status, size_t s, uint8_t pos);
void update_tree(uint64_t man_tile, bool status, size_t s, uint8_t pos);
uint8_t get_max_free_space(const uint64_t* man_tile);

uint8_t tree_depth;
uint64_t root_node;

#endif //DYNAMISCHESSPEICHERDINGS_MY_ALLOC_H
