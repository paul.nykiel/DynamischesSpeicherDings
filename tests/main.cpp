#include <gtest/gtest.h>
#include "../my_alloc.h"

#define TEST_MEM_SIZE 32000000 // Maximum size

#include "VirtualToRealAndInverseMapping.hpp"

int main(int ac, char* av[])
{
    testing::InitGoogleTest(&ac, av);
    return RUN_ALL_TESTS();
}