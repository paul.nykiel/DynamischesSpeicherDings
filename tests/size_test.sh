#!/usr/bin/env bash
data_size=$(size $1 --format=sysv | grep \\.data | grep -o [0-9]* | head -1)
bss_size=$(size $1 --format=sysv | grep \\.bss | grep -o [0-9]* | head -1)
size=$((data_size + bss_size))
echo "Total size: ${size}"
if [ "${size}" -gt "255" ]; then
    echo "Error!" 1>&2
    exit 1
fi
