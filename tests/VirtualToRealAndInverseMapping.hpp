#ifndef DYNAMISCHESSPEICHERDINGS_VIRTUALTOREALANDINVERSEMAPPING_HPP
#define DYNAMISCHESSPEICHERDINGS_VIRTUALTOREALANDINVERSEMAPPING_HPP

TEST(Virtual2Real, Basic) {
    for(size_t c=0; c<TEST_MEM_SIZE; c++) {
        EXPECT_NE(vrtl_ptr_2_real(c), (size_t*)0);
    }
}

TEST(Virtual2Real2Virtual, Basic) {
    for(size_t c=0; c<TEST_MEM_SIZE; c++) {
        EXPECT_EQ(real_ptr_2_vrtl(vrtl_ptr_2_real(c)), c);
    }
}

TEST(Real2Virtual, Error) {
    EXPECT_EQ(real_ptr_2_vrtl(0), (uint64_t)-1);
}

#endif //DYNAMISCHESSPEICHERDINGS_VIRTUALTOREALANDINVERSEMAPPING_HPP
