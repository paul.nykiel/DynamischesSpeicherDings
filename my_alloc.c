#ifndef __MYALLOC_DEF
#define __MYALLOC_DEF
#include "my_alloc.h"

#include <stdio.h>

// Necessary for Thales
#ifndef BLOCKSIZE
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stddef.h>
#include "my_system.h"
#define ON_THALES
#endif

#define PTR_PER_PAGE (BLOCKSIZE/sizeof(size_t))
#define TILE_PER_PAGE (BLOCKSIZE/8)

// Used to buffer some real pointers and their corresponding virtual pointer
#define MAX_MEMORY 256000000
#define BUFFER_OFFSET (MAX_MEMORY/(BLOCKSIZE*PTR_PER_PAGE))
#define BUFFER_FREE_LENGTH (BLOCKSIZE - BUFFER_OFFSET)

#define EXECUTE_ORDER 66        // min tree-width
#define BLOCK_LENGTH 33         // Länge eines Blocks plus der Verwaltungskachel

size_t *list_management;

uint64_t root_node = 33;       // Adresse zum aktuellen Wurzelnode des Baums
uint8_t tree_depth;

uint16_t reverse_lookup_buffer_head = 0;

#ifndef ON_THALES
// Just for testing purposes
void print_list() {
    for(size_t c=0; c<PTR_PER_PAGE; c++) {
        printf("%zu\n", list_management[c]);
        if(list_management[c] == 0) {
            return; // Fails if memory is fragmented
        }
        for(int d=0; d<PTR_PER_PAGE; d++) {
            size_t val = ((size_t*)list_management[c])[d];
            if(val == 0) {
                return; // Fails if memory is fragmented
            }
            printf("\t%zu\n", val);
        }
    }
}

void print_mantile(uint64_t val) {
    for(int c=0; c<64; c++) {
        if(val % 2 == 0) {
            printf("%d", 0);
        } else {
            printf("%d", 1);
        }
        val/=2;
        if(c % 2 == 1) {
            printf(" ");
        }
    }
}

void print_level(uint16_t level, uint64_t node) {
    if(level == 0) {
        uint64_t leftval = *vrtl_ptr_2_real(node - BLOCK_LENGTH);
        uint64_t rightval = *vrtl_ptr_2_real(node + 1);
        print_mantile(leftval);
        printf("\t");
        print_mantile(rightval);
        printf("\t");
    } else {
        uint64_t lchild = get_l_child(node, (uint8_t)level);
        uint64_t rchild = get_r_child(node, (uint8_t)level);
        print_level(level-(uint16_t)1, lchild);
        print_level(level-(uint16_t)1, rchild);
    }
}

void print_tree() {
    print_level(tree_depth, root_node);
}

#endif

/**
 * Funktion um den Pointer im virtuellen Speicher auf echte Pointer zu mappen, ist der Speicher noch nicht
 * allokiert wird das gemacht.
 * @param vrtl_ptr ein virtueller Pointer, startend bei 0
 * @return einen echten Pointer
 */
size_t* vrtl_ptr_2_real(size_t vrtl_ptr) {
    size_t index_vrtl_block = vrtl_ptr/TILE_PER_PAGE;
    size_t index_in_vrtl_block = vrtl_ptr%TILE_PER_PAGE;
    size_t index_in_block = index_vrtl_block%PTR_PER_PAGE;
    size_t index_in_management = index_vrtl_block/PTR_PER_PAGE;

    size_t *block_ptr = (size_t*)list_management[index_in_management];

    // New block for distributed list
    if(block_ptr == 0) {
        block_ptr = (size_t*)get_block_from_system();
        list_management[index_vrtl_block/PTR_PER_PAGE] = (size_t)block_ptr;
        for(int c=0; c<PTR_PER_PAGE; c++) {
            block_ptr[c] = 0;
        }
        block_ptr[index_in_block] = (size_t)get_block_from_system();
    } else if(block_ptr[index_in_block] == 0) {  // New block for virtual memory
        block_ptr[index_in_block] = (size_t)get_block_from_system();
    }

    return (size_t*)(block_ptr[index_in_block] + index_in_vrtl_block*8);
}

/**
 * Versucht einen reellen Pointer zu einem virtuellen Pointer umzurechnen, vorsicht diese Methode ist nicht sehr performant.
 * @param real_ptr ein Echter Pointer der im auch im virtuellen Speicher liegt
 * @return denn zugehörigen virtuellen Pointer falls er existiert sonst (size_t)-1, da die Null im virtuellen Speicher
 * genutzt wird.
 */
size_t real_ptr_2_vrtl(const size_t* real_ptr) {
    // This code is not tested
    /*for(int c=0; c<BUFFER_FREE_LENGTH; c+=2) {
        if(list_management[BUFFER_OFFSET + c] == (size_t)real_ptr) {
            return list_management[BUFFER_OFFSET + c +  1];
        }
    }*/
    // End not tested

    for(size_t c=0; c<PTR_PER_PAGE; c++) {
        if(list_management[c] == 0){
            continue;
        }
        size_t* blk = (size_t*)list_management[c];
        for(size_t d=0; d<PTR_PER_PAGE; d++) {
            size_t* blk_ptr = (size_t*)blk[d];
            size_t diff = real_ptr - blk_ptr; // We accept overflows to reduce a condition in the next line
            if(diff < BLOCKSIZE) {
                return c*PTR_PER_PAGE*TILE_PER_PAGE + d*TILE_PER_PAGE + diff;
            }
        }
    }
    return (size_t)-1;
}

/**
 * Berechnet die Anzahl der Kacheln die ab dem angegebenen virtuellen Pointer noch im selben Systemblock liegen
 * @param vrtl_ptr einen virtuellen Pointer
 * @return die Anzahl der Kacheln nach der aktuellen Kachel (ohne die aktuelle Kachel) die noch im aktuellen Block liegen
 */
uint16_t tiles_to_end(uint64_t vrtl_ptr) {
    uint16_t index_in_page = (uint16_t)(vrtl_ptr % TILE_PER_PAGE);
    return (uint16_t)(TILE_PER_PAGE - index_in_page - 1);
}


uint64_t get_l_child(uint64_t parent, int8_t depth) {
    if(depth < 0){
        return parent - BLOCK_LENGTH;
    }
    return parent - 34 * (1 << depth);
}

uint64_t get_r_child(uint64_t parent, int8_t depth) {
    if(depth < 0){
        return parent + 1;
    }
    return parent + 34 * (1 << depth);
}

uint8_t get_node_val(uint64_t rel_addr) {
    uint8_t * addr = (uint8_t*)vrtl_ptr_2_real(rel_addr); //In 8 bit, ann kann man die Children einzeln auslesen
    return (addr[0] < addr[4] ? addr[4] : addr[0]);
}

uint8_t get_max_free_space(const uint64_t* man_tile) {
    //@TODO optimierung
    uint8_t max_length = 0;
    uint8_t current_length = 0;
/*
    for (int8_t i = 63; i >= 0; i -= 2) {
        uint8_t stat_of_tile = (uint8_t)((*man_tile & ((uint64_t)0b11 << (i)))  >> (i));#
*/
    for (uint8_t i = 0; i < 32; i++) {
        uint8_t shift = (uint8_t)(64 - 2*(1+i));
        uint8_t stat_of_tile = (uint8_t)((*man_tile & ((uint64_t)0b11) << shift) >> shift);

        if (stat_of_tile == 0) {
            current_length++;
        }
        else {
            if (current_length > max_length) {
                max_length = current_length;
            }
            current_length = 0;
        }
    }
    if (current_length > max_length) {
        max_length = current_length;
    }

    return max_length;
}

uint64_t get_leaf_parent(uint64_t leaf) {
    uint64_t current_node = root_node;
    uint8_t depth = tree_depth;


    while (depth > 0) {
        if (current_node < leaf) {
            current_node = get_r_child(current_node, depth - 1);
        }
        else {
            current_node = get_l_child(current_node, depth - 1);
        }
        depth--;
    }

    return current_node;
}

 /**
  * Berechnet die virtuelle Adresse der Parent Node eines beliebigen Nodes in Baum
  * @param node virtuelle Adresse des Child dessen Parent bestimmt werden soll.
  * @param depth Tiefe in der sich das Child im Baum befindet
  * @return virtuelle Adresse der Parent Node
  */
uint64_t get_node_parent(uint64_t node, uint8_t depth) {
    uint64_t x = 0;
    //uint8_t loc_tree_depth = tree_depth + (uint8_t)1;

	if(node == root_node){
		return root_node;
	}

    if (root_node > node) {
        if ((depth % 2) == 0) {
            x = (((root_node - node) / 34) - 1) / 2;
            if ((x % 2) == 0) {
                return node - (34 << ((depth)));
            }
            else {
                return node + (34 << ((depth)));
            }
        }
        else {
            x = ((root_node - node) / 34) / 2;
            if ((x % 2) == 0) {
                return node - (34 << ((depth)));
            }
            else {
                return node + (34 << ((depth)));
            }
        }
    }
    else {
        if ((depth % 2) == 0) {
            x = (-1) * (((root_node - node) / 34) - 1) / 2;
            if ((x % 2) == 0) {
                return node - (34 << ((depth)));
            }
            else {
                return node + (34 << ((depth)));
            }
        }
        else {
            x = (-1) * ((root_node - node) / 34) / 2;
            if ((x % 2) == 0) {
                return node + (34 << ((depth)));
            }
            else {
                return node - (34 << ((depth)));
            }
        }
    }
}

uint8_t update_man_tile(uint64_t* man_tile, bool status, size_t s, uint8_t pos) {
    // allokieren
    if (status) {
        for (uint8_t i = 0; i < s; i++) {
            if (s-i == 1) {
                *man_tile |= ((uint64_t)0b01 << (64-2*(pos+i+1)));
                *man_tile &= ~((uint64_t)0b10 << (64-2*(pos+i+1)));
            }
            else {
                *man_tile |= ((uint64_t)0b10 << (64-2*(pos+i+1)));
                *man_tile &= ~((uint64_t)0b01 << (64-2*(pos+i+1)));
            }
        }
    }
    // free
    else {
        for (uint8_t i = 0; i < s; i++) {
            *man_tile &= ~((uint64_t)0b11 << (64-2*(pos+i+1)));
        }
    }

    return get_max_free_space(man_tile);
}

void update_tree(uint64_t man_tile, bool status, size_t s, uint8_t pos) {
    uint8_t new_free_space = update_man_tile(vrtl_ptr_2_real(man_tile), status, s, pos);
    uint64_t leaf_parent = get_leaf_parent(man_tile);

    //Parent direkt über leaf updaten
    uint8_t* parent_addr = (uint8_t*)vrtl_ptr_2_real(leaf_parent);
    if(man_tile < leaf_parent) {//linkes Child
        parent_addr[0] = new_free_space;
    }
    else{
        parent_addr[4] = new_free_space;
    }

    if(leaf_parent == root_node)
        return;

    uint64_t curr_node = leaf_parent;
    uint64_t curr_parent;
    uint8_t curr_depth = 0;
    while ((curr_parent = get_node_parent(curr_node, curr_depth)) != root_node){

       /* if(get_node_val(curr_node) >= new_free_space) {
            return;
        }*/

        parent_addr = (uint8_t*)vrtl_ptr_2_real(curr_parent);
        if(curr_node < curr_parent) {//linkes Child
            parent_addr[0] = new_free_space;
        }
        else{
            parent_addr[4] = new_free_space;
        }
        curr_node = curr_parent;
        curr_depth++;
    }

    uint8_t* root_addr = (uint8_t*)vrtl_ptr_2_real(root_node);
    root_addr[0] = get_node_val(get_l_child(root_node, tree_depth - 1));
    root_addr[4] = get_node_val(get_r_child(root_node, tree_depth - 1));
}

size_t* get_mem_prt(uint64_t man_tile, size_t s) {
    size_t * real_man_tile_addres = vrtl_ptr_2_real(man_tile);
    uint8_t current_length = 0;
    uint8_t current_best_length = BLOCK_LENGTH - 1;
    uint8_t current_pos = 0;
    uint8_t pos_best_fit = 0xFF;



    for (uint8_t i = 0; i < 32; i++) {
        uint8_t shift = (uint8_t)(64 - 2*(1+i));
        uint8_t stat_of_tile = (uint8_t)((*real_man_tile_addres & ((uint64_t)0b11) << shift) >> shift);

        current_pos++;

        if (stat_of_tile == 0) {
            current_length++;
        }
        else {
            if (current_length == (uint8_t) s) {
                pos_best_fit = current_pos - (uint8_t)s - (uint8_t)1;
                break;
            }
            else if (current_length > (uint8_t) s && current_length < current_best_length) {
                current_best_length = current_length;
                pos_best_fit = current_pos - (uint8_t)s - (uint8_t )1;
            }
            current_length = 0;
        }
    }

    if (current_length >= (uint8_t) s && current_length <= current_best_length) {
        pos_best_fit = current_pos - (uint8_t)s;
    }

    if(pos_best_fit == 0xFF) {//Dateninkonsistenz
        printf("##############\n");
        abort();
    }

    update_tree(man_tile, true, s, pos_best_fit);
    return real_man_tile_addres + pos_best_fit;
}

uint64_t update_child(uint64_t child, uint8_t depth){
    uint64_t lchild = get_l_child(child, depth - 1);
    uint64_t rchild = get_r_child(child, depth - 1);
    uint8_t* child_addr = (uint8_t*)vrtl_ptr_2_real(child);

    child_addr[0] = (child_addr[4] = 32);

    if(depth == 0){ //leafs updaten
        *vrtl_ptr_2_real(rchild) = 0;
        *vrtl_ptr_2_real(lchild) = 0;
        uint16_t free_space_l = tiles_to_end(lchild);
        uint16_t free_space_r = tiles_to_end(rchild);

        if(free_space_l < 32){
            *vrtl_ptr_2_real(lchild) = (uint64_t)(((uint64_t)0b1 << (uint64_t)(64-tiles_to_end(lchild)*2 + 1)) - 1);
            child_addr[0] = (uint8_t)free_space_l;
            return rchild;
        }
        else if(free_space_r < 32){
            *vrtl_ptr_2_real(rchild) = (uint64_t)(((uint64_t)0b1 << (uint64_t)(64-tiles_to_end(rchild)*2 + 1)) - 1);
            child_addr[4] = (uint8_t)free_space_r;
            return lchild;
        }
        return rchild;
    } else { // Nodes updaten
        update_child(lchild, depth - (uint8_t)1);
        return update_child(rchild, depth - (uint8_t)1);
    }
}


uint64_t get_fitting_leaf(size_t s) {

    int8_t depth = tree_depth;
    uint64_t current_node = root_node;


    while (depth-- >= 0){
        uint8_t* children_vals = (uint8_t*)vrtl_ptr_2_real(current_node);
        if(s > children_vals[0] && s > children_vals[4]){
            root_node = root_node * 2 + 1;
            uint8_t* root_addr = (uint8_t*)vrtl_ptr_2_real(root_node);
            root_addr[0] = children_vals[0] > children_vals[4] ? children_vals[0] : children_vals[4];
            root_addr[4] = 32;
            tree_depth++;
            uint64_t right_child = get_r_child(root_node, tree_depth - 1);
            return update_child(right_child,tree_depth - 1);
        }

        if ((children_vals[0] < children_vals[4] && s <= children_vals[0]) || children_vals[4] < s) {
            current_node = get_l_child(current_node, depth);
        }
            // nach rechts weiter absteigen
        else {
            current_node = get_r_child(current_node, depth);
        }
    }
    return  current_node;
}


void init_my_alloc() {
    list_management = (size_t*) get_block_from_system();
    for(size_t c=0; c<PTR_PER_PAGE; c++) {
        list_management[c] = 0;
    }


    // init virtual tree
    root_node = BLOCK_LENGTH;
    uint8_t* root_addr = (uint8_t*)vrtl_ptr_2_real(root_node);
    root_addr[0] = root_addr[4] = 32;
    tree_depth = 0;
    *vrtl_ptr_2_real(root_node + 1) = 0;
    *vrtl_ptr_2_real(0) = 0;

}

void v_free(uint64_t data) {
    uint64_t man_tile = data - (data % 34);
    uint8_t pos_tile = (uint8_t)(data % 34);


    uint8_t len = 0;
    uint8_t shift = (uint8_t)(64 - 2 * (pos_tile + 1 + len));
    uint8_t tile_status = (uint8_t)((*vrtl_ptr_2_real(man_tile) & ((uint64_t)0b11 << shift)) >> shift);
    len++;
    while(tile_status == 2){
        shift = (uint8_t)(64 - 2 * (pos_tile + 1 + len));
        tile_status = (uint8_t)((*vrtl_ptr_2_real(man_tile) & ((uint64_t)0b11 << shift)) >> shift);
        len++;
    }

    update_tree(man_tile, false, len, pos_tile);
}

/**
 * Gibt ein Pointer auf das erste Element eines Speichersegments angegebener Größe zurück.
 * @param s Größe in bytes
 * @return einen Pointer auf das erste Element des Speichersegments
 */
void* my_alloc(size_t s) {
    if(!(s % 8))
        s /= 8;
    else
        s = s / 8 + 1;
    return get_mem_prt(get_fitting_leaf(s), s);

    // TODO get Working with the Rest
   /* size_t realPtr = 0;
    uint64_t vrtlPtr = 0;
    list_management[BUFFER_OFFSET + reverse_lookup_buffer_head] = realPtr;
    list_management[BUFFER_OFFSET + reverse_lookup_buffer_head + 1] = vrtlPtr;

    reverse_lookup_buffer_head = (reverse_lookup_buffer_head +2) % BUFFER_FREE_LENGTH;*/
}

/**
 * Gibt den mit my_alloc belegten Speicher wieder frei
 * @param ptr ein Pointer auf ein Segement das mit my_alloc belegt wurde
 * @see my_alloc
 */
void my_free(void* ptr) {
    v_free(real_ptr_2_vrtl(ptr));
}

#endif
