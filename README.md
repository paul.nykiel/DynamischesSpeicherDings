# Dynamisches Speicher Dings

[![pipeline status](https://gitlab.com/paul.nykiel/DynamischesSpeicherDings/badges/master/pipeline.svg)](https://gitlab.com/paul.nykiel/DynamischesSpeicherDings/commits/master)

## Toolchain
CMake, Make und GCC. Optimalerweise CLion. GTest für Unit-Tests.

### Installation von GTest

To do unit-testing you need to install GTest.

Download the source and install the dependencies:
    
    sudo apt-get install libgtest-dev


Next change to source-directoy:

    cd /usr/src/gtest


Next run CMake to configure the project: 
    
    sudo cmake CMakeLists.txt


Compile the project:    
    
    sudo make


Finally install GTest:

    sudo cp *.a /usr/lib

## Benutzung von GTest
Zum Unit-Testing verwenden wir GTest, alle Test Files liegen im Verzeichniss tests.
Um ein neues Modul zu testen sollte eine neue Datei modulName.hpp in diesem Verzeichnis angelegt werden, diese
Header-Datei muss dann auch in der tests/main.cpp inkludiert werden und (wenn CLion das nicht automatisch macht)
in die tests/CMakeLists.txt aufgenommen werden. In der Header Datei werden dann die Tests definiert, hierzu gibt
es die Struktur TEST(Testname, Testfall). Der Testname sollte gleich des Dateinamens und der Funtktion sein, der
Testfall kann zum Beispiel BasicInput, ErrorHandling oder ähnliches sein. Z.b. für die Methode sqrt könnte das so aussehen:

<<<README_COPY_START>>>
## Algorithmus (Baum basierend)
### Kachel
Der Speicher wird jeweils in 8-bytes aufgeteilt (eine Kacheln), kleinere
Einheiten gibt es nicht (auch wenn nur ein char benötigt wird werden intern
8 bytes allokiert). Nach Aufgabenstellung werden auch nur durch 8-Teilbare Speicherbereiche allokiert.

### Block
Diese Kacheln werden nun zu Blöcken von 32 Kacheln zusammengefasst.

### Verwaltungskachel
Zu jedem Speicherblock gehört eine Verwaltungskachel. Die Verwaltungskachel besteht wieder
aus 8-bytes bzw 64 bit. Die Verwaltungskachel gibt an, ob die jeweilige Speicherkachel
des Speicherblocks frei ist, außerdem wird gespeichert ob die Folgende Speicher Kachel
ebenfalls zum selben Block dazugehört (so kann free() ohne Angabe einer Länge
aufgerufen werden).

#### Codierung der Informationen in einer Verwaltungskachel
Jede Kachel wird von zwei Bits verwaltet, die deren Zustand signalisieren.

| Wert | Bedeutung          |
| ---  | ---                |
| 0    | Kachel ist frei |
| 1    | Kachel ist belegt und nachfolgende Kachel gehört zu anderer Datenstruktur |
| 2    | Kachel ist belegt und nachfolgende Kachel gehört zur selben Datenstrukutr |
| 3    | Kachel ist belegt und es existiert kein reeler Speicher zu der Kachel (siehe Probleme mit zusammenhängendem Speicher) |

Eventuell können die reservierten Zustände zur Optimierung genutzt werden.

## Einheit
Ein Block mit Verwaltungskachel heißt eine [Einheit](http://aul12.me/DynamischeSpeicherverwaltung/Speichereinheit.png).

## Verwaltungsnode
Nun werden 2 Einheiten wieder zusammengefasst und mit einer Verwaltungskachel versehen.
Diese speichert wieder ob die Einheiten Platz frei haben, und wenn ja wieviel zusammenhängender Speicher maximal verfügbar ist.
Hierfür sind 5-bit pro Child vorgesehen (da maximal 2^5 = 32 Kacheln zusammenhängen).
Die übrigen 54 Bit werden Eventuell noch genutzt, um eine Art Checksum zu speichern (gewisse Zustände werden z.B. nicht genutzt, werden
sie genutzt kann festgestellt werden das die Kachel überschrieben wurde und ein Segfault kann ausgelöst werden).

Diese Verwaltungsnodes können dann wieder zusammengefasst werden und von einer neuen Verwaltungsnode verwaltet werden.
Dadurch entsteht ein Binär-Baum.

## Implementierung des Baumes
Da die Aufgabenstellung eine maximale allokierte Größe von 256-bytes vorgibt,
was weniger weniger als der maximale Speicher ist von 32 Kacheln ist, werden die Kacheln
direkt im Speicher hinterlegt.

## Vorgehen von malloc
Beim allokieren von neuem Speicher wird am Wurzelknoten angefangen und die Child-Node, mit
dem minimalen zusammenhängenden Speicher, in den die Daten noch passen, gesucht. Dann wird
mit dem gleichen Verfahren rekursiv weitergesucht bis eine Verwaltungskachel gefunden wurde.
Diese ermittlet dann die Speicheradresse, an der die Daten gespeicher werden können.

Wenn der zur Verfügung gestellt Speicher nicht mehr ausreicht, wird der aktuelle
Wurzelknoten einfach als weitere Kindknoten aufgefasst und ein neuer
Wurzelknoten mit zusätzlichen Kindknoten mit neuem Speicher angelegt. Um das
möglichst effizient zu gestalten, sollte der Baum in In-Order Reihenfolge im Speicher repräsentiert werden.

## Zusammenfassung des Gesamtspeichers
Um das Arbeiten auf dem Baum zu erleichtern, wird nur mit virtuellen, relativen Adressen gearbeitet.
Um diese virtuellen Addressen auf echte Addressen zu mappen, wird eine verteilte Liste mit allen vom
System angeforderten Blöcken benötigt.

### Probleme durch Zusammenhängenden Speicher
Da der virtuelle Speicher zusammenhängend ist kann beim allokieren nicht erkannt werden ob der vergebene Speicher
vollständig in einem Systemblock liegt. Ist das nicht der Fall wird im Moment ein Pointer auf ein Element am Ende
des ersten Speicherblocks vergeben, da der Nutzer aber mit echten Pointern rechnet wird danach auf Daten zugegriffen
die nach dem Systemblock im noch nicht allokierten Speicher liegen was nicht gewünscht ist.

#### Lösungsansatz
Betrachte das Ende des ersten Blocks (In diesem Beispiel mit einer Blocksize von 8192, Werte Adressen stimmen vermutlich
nicht).
<table>
    <tr>
        <td>Addresse</td>
        <td>...</td>
        <td>8190</td>
        <td>8191</td>
        <td>Virtuell</td>
        <td>...</td>
        <td>8192</td>
        <td>8193</td>
        <td>...<td>
    </tr>
    <tr>
        <td>Inhalt</td>
        <td>...</td>
        <td><b>Verwaltungskachel N</b><td>
        <td>Kachel N*32</td>
        <td>Kacheldauerhaft belegt</td>
        <td>...</td>
        <td><b>Verwaltungskachel N+1</b></td>
        <td>Kachel (N+1)*32</td>
        <td>...<td>
    </tr>
</table>
Das heißt beim initialen Aufbau des Baums muss beachtet werden welche Verwaltungskacheln Speicher über das Ende eines 
Systemblocks verwaltet, hierfür wird die Methode 
    uint16_t tiles_to_end(uint64_t vrtl_ptr) 
benutzt.

### Verteilte Liste
Zur Verwaltung der Verteilten Liste wird ein Block vom System allokiert (der Listenverwaltungsblock). 
Dieser Block beeinhaltet ein Array aus Pointern
auf Listenblöcke (Teile der Liste aller Pointer).
In diesen Listenblöcke sind nun Pointer auf den verwendbaren Speicherblöcke (vom System) hinterlegt.

## Optimierungsoptionen
Um malloc zu beschleunigen kann eventuell der noch frei globale Speicher genutzt werden um eine Tabelle mit
Pointern auf Kacheln mit definiertem freien Platz zu speichern.



## Bilder
### Skizzen
[http://aul12.me/DynamischeSpeicherverwaltung/](http://aul12.me/DynamischeSpeicherverwaltung/)
### Verteilte Liste zur Verwaltung der Systemblöcke
Mit ->n angegebene Werte sind Pointer auf *n.
<table>
    <tr>
        <td colspan="4">Listenverwaltungsblock</td>
        <td>...</td>
        <td colspan="4">Erster Listenblock (*1)</td>
        <td></td>
        <td colspan="4">Zweiter Listenblock (*2)></td>
        <td>...</td>
    </tr>
    <tr>
        <td>Pointer auf ersten Listenblock (->1)</td>
        <td>Pointer auf zweiten Listenblock (->2)</td>
        <td>...</td>
        <td>Pointer auf letzten Listenblock (maximal 64)</td>
        <td>...</td>
        <td>Pointer auf ersten Speicherblock</td>
        <td>Pointer auf zweiten Speicherblock</td>
        <td>...</td>
        <td>Pointer auf 1000. Speicherblock</td>
        <td></td>
        <td>Pointer auf 1001 Speicherblock</td>
        <td>Pointer auf 1002 Speicherblock</td>
        <td>...</td>
        <td>Pointer auf 2000. Speicherblock</td>
        <td>...</td>
    </tr>
</table>

### Speicher Baum (im virtuellen, zusammenhängenden Speicher)
Texte in Fett, sind Verwaltungsdaten. Die Nodes des Baums werden mit N mit Index bezeichnet.
Die Root-Node heißt N0, die erste Childnode von Nk heißt Nk0, die zweite Childnode Nk1.

Repräsentation im Speicher:
<table>
    <tr>
        <td>Addresse</td>
        <td>0</td>
        <td>1</td>
        <td>...</td>
        <td>32</td>
        <td>33</td>
        <td>34</td>
        <td>35</td>
        <td>...</td>
        <td>66</td>
        <td>67</td>
        <td>68</td>
        <td>69</td>
        <td>...</td>
        <td>100</td>
        <td>101</td>
        <td>102</td>
        <td>103</td>
        <td>...</td>
        <td>134</td>
    </tr>
    <tr>
        <td>Inhalt</td>
        <td><b>N000 Verwaltungskachel</b></td>
        <td>Kachel 0</td>
        <td>...</td>
        <td>Kachel 31</td>
        <td><b>N00 Verwaltungskachel</b></td>
        <td><b>N001 Verwaltungskachel</b></td>
        <td>Kachel 0</td>
        <td>...</td>
        <td>Kachel 31</td>
        <td><b>N0 Verwaltungskachel (Root-Node)</b></td>
        <td><b>N010 Verwaltungskachel</b></td>
        <td>Kachel 0</td>
        <td>...</td>
        <td>Kachel 31</td>
        <td><b>N01 Verwaltungskachel</b></td>
        <td><b>N011 Verwaltungskachel</b></td>
        <td>Kachel 0</td>
        <td>...</td>
        <td>Kachel 31</td>
    </tr>
</table>

Zugehöriger Baum:
<table>
    <tr>
        <td colspan="12">Verwaltungskachel N0</td>
    </tr>
    <tr>
        <td colspan="6">Verwaltungskachel N00</td>
        <td colspan="6">Verwaltungskachel N01</td>
    </tr>
    <tr>
        <td colspan="3">N000 Verwaltungskachel</td>
        <td colspan="3">N001 Verwaltungskachel</td>
        <td colspan="3">N010 Verwaltungskachel</td>
        <td colspan="3">N011 Verwaltungskachel</td>
    </tr>
    <tr>
        <td>Kindkachel 0</td>
        <td>...</td>
        <td>Kindkachel 31</td>
        <td>Kindkachel 0</td>
        <td>...</td>
        <td>Kindkachel 31</td>
        <td>Kindkachel 0</td>
        <td>...</td>
        <td>Kindkachel 31</td>
        <td>Kindkachel 0</td>
        <td>...</td>
        <td>Kindkachel 31</td>
    </tr>
</table>

<<<README_COPY_END>>>
## Wörtersammlung für  Name (rekursives Akronym)
GNE - GNE is not effective

 * Memory, Management
 * Fast
 * Tree
 * Node
 * Logarithmic
 * Allocate, Algorithm
 * Whisky
 * Distributed (Echtzeit Java), Dynamic
 * In-Order
 * Effective