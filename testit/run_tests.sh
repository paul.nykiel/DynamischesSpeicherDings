#!/bin/bash
MAX_SEED=$1
MAX_COUNT=$2

for seed in $(seq 0 ${MAX_SEED})
do
    for count in $(seq 0 ${MAX_COUNT})
    do
        echo "\n\nRunning testit with seed=$seed, count=$count"
        START=$(date +%s.%N)
        ./TestIt "$seed" "$count"
        END=$(date +%s.%N)
        DIFF=$(echo "$END - $START" | bc)
        echo "Execution took: ${DIFF} seconds"
    done
done