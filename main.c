#include <stdio.h>
#include <stdint.h>

#include "my_alloc.h"

#include <sys/time.h>

//#define TEST_MEM_SIZE 32000000 // Maximum size
#define TEST_MEM_SIZE 1000000//500000

size_t ptrs[TEST_MEM_SIZE];

int main() {
    init_my_alloc();
/*
    int* i = my_alloc(sizeof(int)*40);

    printf("i prt: %zu\n",(size_t) i);

    for( int b=0;b<40;b++){
        i[b]=b;
    }
    for (int a = 0; a < 40; a++) {
        //printf("das %d. Element ist: %d\n", i[a], i[a]);
    }

    //my_free(i);

    int* c =my_alloc(sizeof(int)*40);

    printf("c prt: %zu\n",(size_t) c);
    /*
    for(size_t c=0; c<65536; c++) {
        printf("%zu\n", (size_t )my_alloc(8));
    }*/
    //my_alloc(sizeof(int)*40);
    //print_tree();

    /*my_alloc(32*8);
    my_alloc(32*8);
    my_alloc(8);
    volatile int i = 0;*/

    for(int n=0; n<1000; n++) {
        for (int c = 0; c < 256; c++) {
            my_alloc((size_t)c);
            //print_tree();
        }
    }

    //my_alloc(16);
    //free(d);
}

/*
long long current_timestamp() {
    struct timeval te;
    gettimeofday(&te, NULL); // get current time
    long long milliseconds = te.tv_sec*1000LL + te.tv_usec/1000; // calculate milliseconds
    return milliseconds;
}

int main() {
    init_my_alloc();
    long long start, end;
    printf("Memory-Size:\t %d\n", TEST_MEM_SIZE*8);
    printf("Access with Alloc:\t");
    start = current_timestamp();
    for(size_t c=0; c<TEST_MEM_SIZE; c++) {
        vrtl_ptr_2_real(c);
    }
    end = current_timestamp();
    printf("%lli ms\n", end-start);

    printf("List:\n");
    // print_list();

    printf("Pure Access:\t");
    start = current_timestamp();
    for(size_t c=0; c<TEST_MEM_SIZE; c++) {
        ptrs[c] = (size_t)vrtl_ptr_2_real(c);
    }
    end = current_timestamp();
    printf("%lli ms\n", end-start);

    printf("Reverse Lookup (<=> normal free):\t");
    start = current_timestamp();
    for(size_t c=0; c<TEST_MEM_SIZE; c++) {
        real_ptr_2_vrtl((size_t*)ptrs[c]);
    }
    end = current_timestamp();
    printf("%lli ms\n", end-start);

    printf("Buffered Free:\t");
    start = current_timestamp();
    for(size_t c=0; c<TEST_MEM_SIZE; c++) {
        my_free((size_t*)ptrs[c]);
    }
    end = current_timestamp();
    printf("%lli ms\n", end-start);

    return 0;
}
 */
